import logging
import dateutil.parser
import urllib.parse
import bs4


class Message:
    """ Class describing an e-boks message. """
    def __init__(self):

        # Unique message identification number. The id cannot change and persists for the lifetime of the message.
        self.id = ''

        # The message name.
        self.name = ''

        # Flag to see if the message is unread.
        self.unread = False

        # The date and time that the message was received. Is an instance of the Datetime class.
        self.receive_datetime = None

        # The unique identification number of the folder that the message is inside.
        self.folder_id = ''

        # The size in bytes of the message content.
        self.size_bytes = 0

        # The file type of the message, can be any 1-4 letter string (pdf, txt, html, exe, etc.).
        self.filetype = ''

        # The unique identification number of the one who sent the message. This will be '0' if it was sent/uploaded
        # by yourself.
        self.sender_id = ''

        # The name of the one who sent the message.
        self.sender_name = ''

        # The amount of attachments to the message. Generally a message only have one main text. Attachments
        # are additional bodies of text. Attachments are fetched separately from the main message.
        self.attachments_count = 0

        # Optional note to the message. This will be empty if you have not given the message a note.
        self.note = ''


def get_latest(session):
    """ Get a list of the latest messages.

    We do not yet know how it finds out how many messages to return. I get 16 back each time.
    """
    url = '{uri}/0/mail/folder/search/latest'.format(uri=session.uri)
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return extract_messages_from_xml(body)


def get_from_period(session, start, end):
    """ Get a list of messages between a start and end date.

    The start and end date should be datetime objects.
    """
    url = '{uri}/0/mail/folder/search/latest?from={start}&to={end}'.format(
        uri=session.uri,
        start=start.isoformat(),
        end=end.isoformat()
    )
    response = session.get(url)
    body = bs4.BeautifulSoup(response.text, 'html.parser')
    return extract_messages_from_xml(body)


def get_unread(session):
    """ Iterate over all unread messages. """
    url = '{uri}/0/mail/folder/search/unread?'.format(uri=session.uri)
    return iterate_over_messages(session, url)


def get_from_sender(session, sender):
    """ Iterate over all messages from a sender.

    Set sender to eboks.sender.myself() to get messages uploaded by you.
    """
    url = '{uri}/0/mail/folder/search/senders/{sender_id}?'.format(uri=session.uri, sender_id=sender.id)
    return iterate_over_messages(session, url)


def get_from_folder(session, folder):
    """ Iterate over all messages in a folder. """
    url = '{uri}/0/mail/folder/{folder_id}?latest=false&'.format(uri=session.uri, folder_id=folder.id)
    return iterate_over_messages(session, url)


def search(session, string):
    """ Iterate over all messages that has a specific string in its title. """
    url = '{uri}/0/mail/folder/search?search={string}&'.format(uri=session.uri, string=urllib.parse.quote(string))
    return iterate_over_messages(session, url)


def iterate_over_messages(session, base_url, take=50):
    """ Generator function that iterates over messages returned from e-boks rest api queries.

    Most e-boks api methods that return a list of messages requires a skip and take parameter. Skip is the amount of
    messages to jump over and take is the amount of message to return. By modifying skip it is possible to go though
    thounsands of message by taking take messages at a time.

    In python we have implemented this skip and take with a generator function, so it is possible to just do a for
    loop over all messages.

    Pass in an url for the resource that you want fetched, the url will get the skip and take parameters appended
    before being sent.
    """
    skip = 0
    while True:
        url = base_url + 'skip={skip}&take={take}'.format(skip=skip, take=take)
        response = session.get(url)
        body = bs4.BeautifulSoup(response.text, 'html.parser')
        messages = extract_messages_from_xml(body)
        for message in messages:
            yield message
        if len(messages) < take:
            return
        skip += len(messages)


def extract_messages_from_xml(xml):
    """ Convert an xml list of messages to to a list of Message() instances. """
    messages = []
    for message_xml in xml.find('messages'):
        message = extract_message_from_xml(message_xml)
        messages.append(message)
    return messages


def extract_message_from_xml(message_xml):
    """ Convert a single xml messageinfo structure to a Message() instance. """
    message = Message()
    message.id = message_xml['id']
    message.name = message_xml['name']
    message.unread = string_to_bool(message_xml['unread'])
    message.receive_datetime = dateutil.parser.parse(message_xml['receiveddatetime'])
    message.size_bytes = int(message_xml['size'])
    message.filetype = message_xml['format']
    message.folder_id = message_xml.get('folderid')
    message.attachments_count = int(message_xml['attachmentscount'])
    if message_xml.note:
        message.note = message_xml.note.string

    # No sender id means that it was uploaded by the user, which is sender id '0'.
    message.sender_name = message_xml.sender.string
    message.sender_id = message_xml.sender.get('id')
    if not message.sender_id:
        message.sender_id = '0'
    return message


def get_content(session, message):
    """ Get a message's content as a string.

    The requests library will try to pick the best decoding option. """
    return get_content_impl(session, message).text


def get_content_bytes(session, message):
    """ Get a message's content as a raw byte array. """
    return get_content_impl(session, message).content


def get_content_impl(session, message):
    """ Get a response object containing a messsage's content. """
    url = '{uri}/0/mail/folder/{folder_id}/message/{message_id}/content'.format(
        uri=session.uri,
        folder_id=message.folder_id,
        message_id=message.id
    )
    response = session.get(url)
    return response


def upload_message(session, folder, name, filetype, message):
    """ Send a message to your e-boks.

    The sender will be yourself, with sender id '0'.

    :param session: session to use for the request
    :param folder: folder to put the message in
    :param name: title of the message
    :param filetype: 1-4 letter word describing the filetype (for example pdf, xml, html)
    :param message: the message text
    """
    url = '{uri}/0/mail/folder/{folder_id}?name={filename}&filetype={filetype}'.format(
        uri=session.uri,
        folder_id=folder.id,
        filename=urllib.parse.quote(name),
        filetype=urllib.parse.quote(filetype)
    )
    session.put(url=url, headers={'Content-Type': 'application/octet'}, body=message)
    logging.info('Uploaded message: ' + name)
    return find_uploaded_message(session, name, filetype)


def upload_file(session, folder, filename, filetype):
    """ Send a file to your e-boks.

    The sender will be yourself, with sender id '0'.

    :param session: session to use for the request
    :param folder: folder to put the file in
    :param filename: the filename for the file to upload
    :param filetype: 1-4 letter word describing the filetype (for example pdf, xml, html)
    """
    with open(filename, 'rb') as body:
        url = '{uri}/0/mail/folder/{folder_id}?name={filename}&filetype={filetype}'.format(
            uri=session.uri,
            folder_id=folder.id,
            filename=urllib.parse.quote(filename),
            filetype=urllib.parse.quote(filetype)
        )
        session.put(url=url, headers={'Content-Type': 'application/octet-stream'}, body=body)
    logging.info('Uploaded file: ' + filename)
    return find_uploaded_message(session, filename, filetype)


def find_uploaded_message(session, name, filetype):
    for message in get_latest(session):
        if message.name == name and message.filetype == filetype:
            return message
    raise LookupError('Unable to find uploaded message named ' + name)


def delete(session, message):
    """ Delete a message in your e-boks.

    If the message is in the deleted items folder, the message will be permanently deleted. If it is in any other
    folder it will be moved to the deleted items folder, where it is permanently deleted after 30 days.

    For development purposes we only allow deletion of messages that have been uploaded by yourself. We don't want
    to accidentally delete important mail.
    """
    if message.sender_id != '0':
        raise ValueError('We only allow deletion of messages uploaded by yourself.')
    url = '{uri}/0/mail/folder/{folder_id}/message/{message_id}'.format(
        uri=session.uri,
        folder_id=message.folder_id,
        message_id=message.id
    )
    session.delete(url)
    logging.info('Deleted message ' + message.name + ' with id ' + message.id)


def rename(session, message, newname):
    """ Change the title of a message. """
    if message.sender_id != '0':
        raise ValueError('We only allow renaming of messages uploaded by yourself.')
    url = '{uri}/0/mail/folder/{folderid}/message/{message_id}?newName={newname}'.format(
        uri=session.uri,
        folderid=message.folder_id,
        message_id=message.id,
        newname=urllib.parse.quote(newname)
    )
    session.put(url)
    message.name = newname


def move(session, message, destination_folder):
    """ Move a message to another folder. """
    url = '{uri}/0/mail/folder/{folder_id}/message/{message_id}?toFolderID={destination_folder_id}'.format(
        uri=session.uri,
        folder_id=message.folder_id,
        message_id=message.id,
        destination_folder_id=destination_folder.id
    )
    session.put(url)
    message.folder_id = destination_folder.id


def set_note(session, message, note):
    """ Set or add a note to the message. Overwrites the current note. """
    url = '{uri}/0/mail/folder/{folderid}/message/{messageid}?note={note}'.format(
        uri=session.uri,
        folderid=message.folder_id,
        messageid=message.id,
        note=urllib.parse.quote(note)
    )
    session.put(url)
    message.note = note


def set_read(session, message, read=True):
    """ Set a message as having been read.

    Setting read to False will mark the message as unread.
    """
    url = '{uri}/0/mail/folder/{folder_id}/message/{message_id}?read={read}'.format(
        uri=session.uri,
        folder_id=message.folder_id,
        message_id=message.id,
        read=bool_to_string(read)
    )
    session.put(url)
    message.unread = not read


def string_to_bool(string):
    if string == 'true':
        return True
    elif string == 'false':
        return False
    raise ValueError()


def bool_to_string(boolean):
    if boolean:
        return 'true'
    else:
        return 'false'
