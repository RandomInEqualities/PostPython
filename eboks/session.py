import datetime
import hashlib
import re
import os.path
import configparser

import requests
import requests.auth
import bs4


class EboksAuth(requests.auth.AuthBase):
    """ Custom requests authentication class.

    It sets the e-boks 'X-EBOKS-AUTHENTICATE' authentication header to the correct value before each request.
    """
    def __init__(self, get_auth_string_function):
        self.get_auth_string_function = get_auth_string_function

    def __call__(self, request):
        request.headers['X-EBOKS-AUTHENTICATE'] = self.get_auth_string_function()
        return request


class EboksSession:
    """ Interface to the mobile e-boks rest api.

    Takes care of authentication and allows you to query the rest api with GET, PUT and DELETE requests.
    """

    def __init__(self):

        # Your cpr number.
        self.cpr = ''

        # Your mobile e-boks password.
        self.password = ''

        # Your mobile e-boks activation code.
        self.activation = ''

        # The country for e-boks access. Sweden also has e-boks, but we have not tested it with any swedish accounts.
        self.country = 'DK'

        # The type of access. E-boks is also for companies, where each company can get a separate e-boks account.
        # We have only tested it with regular accounts.
        self.type = 'P'

        # The device id of the machine that is running the e-boks 'mobile app'. We have set it to a random string.
        self.deviceid = '5a506c29-0524-40f6-928f-f43f2b2bbf76'

        # The root location of the e-boks rest api. It is https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/{user_id}.
        self.uri = ''

        # The url for establishing an e-boks session.
        self.url_session = 'https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/session'

        # The session id, returned after a successful logon.
        self.sessionid = ''

        # The cryptographic nonce. It is sent with each request and updated on each response.
        self.nonce = ''

        # The constant challenge response authentication string used between logon and logoff.
        self.challenge_response = ''

        # The user name of the e-boks account.
        self.username = ''

        # The user id of the e-boks account.
        self.userid = ''

        # The requests session object that we use to make actual http requests. (don't use this explicitly).
        self.session = None

    def logon(self):
        """ Log into an e-boks account. Will ask for cpr number, password and activation code.

        Will raise requests.HTTPError if the logon fails.
        """
        if not self.cpr or not self.password or not self.activation:
            self.cpr, self.password, self.activation = get_user_credentials('credentials.txt')

        # Create a session and set the default headers and authentication method.
        self.session = requests.Session()
        self.session.auth = EboksAuth(self.get_auth_string)
        self.session.verify = True
        self.session.headers = {
            'User-Agent': 'okhttp/2.4.0',
            'Host': 'rest.e-boks.dk',
        }

        # Construct logon request.
        headers = {'Content-Type': "text/xml"}
        body = read_xml_file('logon.xml').format(
            cpr=self.cpr,
            type=self.type,
            country=self.country,
            password=self.password
        )

        # Send logon request, use logon authentication.
        response = self.session.put(
            self.url_session,
            headers=headers,
            data=body,
            auth=EboksAuth(self.get_auth_string_logon)
        )
        response.raise_for_status()

        # Extract session info.
        response_xml = bs4.BeautifulSoup(response.text, 'html.parser')
        self.username = response_xml.session.user['name']
        self.userid = response_xml.session.user['userid']
        self.nonce = get_nonce(response)
        self.sessionid = re.search('sessionid="([a-f0-9\-]+)"', response.headers['X-EBOKS-AUTHENTICATE']).group(1)
        self.uri = 'https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/' + self.userid
        self.challenge_response = self.get_challenge_response()

    def logoff(self):
        """ Log out of the current session. """
        self.session.delete(self.url_session, auth=EboksAuth(self.get_auth_string_logoff))
        self.session.close()

    def get(self, url, headers=None, body=None):
        """ Submit a GET request to the e-boks rest api. Must be logged in before calling.

        Will raise requests.HTTPError if the query fails.

        :return: A requests.response object
        """
        return self.send(self.session.get, url, headers, body)

    def put(self, url, headers=None, body=None):
        """ Submit a PUT request to the e-boks rest api. Must be logged in before calling.

        Will raise requests.HTTPError if the query fails.

        :return: A requests.response object
        """
        return self.send(self.session.put, url, headers, body)

    def delete(self, url, headers=None, body=None):
        """ Submit a DELETE request to the e-boks rest api. Must be logged in before calling.

        Will raise requests.HTTPError if the query fails.

        :return: A requests.response object
        """
        return self.send(self.session.delete, url, headers, body)

    def send(self, request_function, url, headers, body):
        """ Send a request to the e-boks server. """
        response = request_function(url=url, headers=headers, data=body)
        if have_authentication_header(response):
            self.nonce = get_nonce(response)
        response.raise_for_status()
        return response

    def get_auth_string(self):
        """ Authentication string used between logon and logoff. """
        authentication = ','.join([
            'deviceid=' + self.deviceid,
            'nonce=' + self.nonce,
            'sessionid=' + self.sessionid,
            'response=' + self.challenge_response
        ])
        return authentication

    def get_auth_string_logon(self):
        """ Authentication string used in logon. """
        time = datetime.datetime.now().replace(microsecond=0).isoformat(' ')
        authentication = ','.join([
            'deviceid=' + self.deviceid,
            'datetime=' + time,
            'challenge=' + self.get_challenge_logon(time)
        ])
        return 'logon ' + authentication

    def get_auth_string_logoff(self):
        """ Authentication string used in logoff. """
        authentication = ','.join([
            'deviceid=' + self.deviceid,
            'nonce=' + self.nonce,
            'sessionid=' + self.sessionid,
            'response=' + self.get_challenge_response()
        ])
        return 'logoff ' + authentication

    def get_challenge_logon(self, time):
        """ The challenge-response authentication used in logon. """
        raw = ':'.join([
            self.activation,
            self.deviceid,
            self.type,
            self.cpr,
            self.country,
            self.password,
            time
        ])
        return double_hash_sha256_hex(raw)

    def get_challenge_response(self):
        """ The challenge-response authentication used for every request after logon. """
        raw = ':'.join([
            self.activation,
            self.deviceid,
            self.nonce,
            self.sessionid
        ])
        return double_hash_sha256_hex(raw)


def get_user_credentials(credentials_filename):
    """ Ask user for cpr, password and activation code for an e-boks account.

    Will look though a credentials file first, to see if the user have stored credentials there. In this way the user
    do not have to type everything on every logon.

    Otherwise it just asks for the three credentials.
    """
    credentials = configparser.ConfigParser()
    credentials.read(credentials_filename)

    cpr = credentials.get('e-boks.dk', 'cpr', fallback='')
    password = credentials.get('e-boks.dk', 'password', fallback='')
    activation = credentials.get('e-boks.dk', 'activation', fallback='')

    if not cpr:
        cpr = input('Enter your cpr: ')
    if not password:
        password = input('Enter your password: ')
    if not activation:
        activation = input('Enter your activation code: ')

    if 'e-boks.dk' not in credentials:
        ask_user_for_storage_permission(credentials, credentials_filename, cpr, password, activation)

    return cpr, password, activation


def ask_user_for_storage_permission(credentials, credentials_filename, cpr, password, activation):
    """ Ask if the user want to store its cpr, password and activation code in a file.

    It is no better than storing passwords in firefox, chrome etc. It can only be stored in plain text.
    """
    use_storage = input(
        'Do you want to store your cpr, password and activation code in PLAIN TEXT? It will be \n' +
        'stored in {0} in the current working directory. (yes/no): '.format(credentials_filename)
    )

    credentials['e-boks.dk'] = {}
    credentials['e-boks.dk']['use_storage'] = use_storage

    if use_storage.lower() == 'yes':
        credentials['e-boks.dk']['cpr'] = cpr
        credentials['e-boks.dk']['password'] = password
        credentials['e-boks.dk']['activation'] = activation

    with open(credentials_filename, 'w') as storage:
        credentials.write(storage)


def have_authentication_header(response):
    """ Check that the e-boks response have an authentication header. """
    return 'X-EBOKS-AUTHENTICATE' in response.headers


def get_nonce(response):
    """ Extract the next nonce from the e-boks authentication header. """
    return re.search('nonce="([a-f0-9]+)"', response.headers['X-EBOKS-AUTHENTICATE']).group(1)


def double_hash_sha256_hex(string):
    single_hash = hashlib.sha256(string.encode()).hexdigest()
    double_hash = hashlib.sha256(single_hash.encode()).hexdigest()
    return double_hash


def read_xml_file(name):
    """ Read a file from disk and return its content as a string. """
    module_dir = os.path.dirname(os.path.abspath(__file__))
    xmlpath = os.path.join(module_dir, name)
    with open(xmlpath) as xmlfile:
        return xmlfile.read()
