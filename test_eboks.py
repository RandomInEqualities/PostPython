import unittest
import datetime
import logging
import requests
import os

import eboks

logging.basicConfig(
    filename='test_eboks.log',
    filemode='a',
    level=logging.INFO,
    format='%(asctime)s %(levelname)s:%(message)s'
)


class TestEboksSession(unittest.TestCase):
    """ Class for testing an e-boks session. """
    
    def setUp(self):
        self.session = eboks.EboksSession()
        self.session.logon()

    def tearDown(self):
        self.session.logoff()

    def test_session_get(self):
        response_1 = self.session.get(self.session.uri + '/0/mail/folders')
        self.assertEqual(response_1.status_code, 200)
        
        response_2 = self.session.get(self.session.uri + '/0/mail/folders')
        self.assertEqual(response_2.status_code, 200)
        
        response_3 = self.session.get(self.session.uri + '/0/mail/folders')
        self.assertEqual(response_3.status_code, 200)

    def test_session_get_invalid(self):
        self.assertRaises(requests.HTTPError, self.session.get, self.session.uri + '/blblblblblb/mail')


class TestEboksSender(unittest.TestCase):
    """ Class for testing e-boks senders. """
    
    def setUp(self):
        self.session = eboks.EboksSession()
        self.session.logon()

    def tearDown(self):
        self.session.logoff()

    def test_get_senders(self):
        senders = eboks.sender.get_senders(self.session)
        for sender in senders:
            self.assertTrue(sender.id.isdigit())
            self.assertGreater(len(sender.name), 0)

    def test_sender_get_message_total(self):
        # Get the amount uploaded by myself.
        total = eboks.sender.get_message_total(self.session, eboks.sender.get_myself())
        self.assertGreaterEqual(total, 0)


class TestEboksCustomer(unittest.TestCase):
    """ Class for testing e-boks customers.

    We do not test subscribing and unsubscribing to customers as it can mess up the eboks account.
    """

    def setUp(self):
        self.session = eboks.EboksSession()
        self.session.logon()

        # A customer that we can do operations on.
        self.danske_bank = eboks.customer.Customer()
        self.danske_bank.id = '1033'
        self.danske_bank.name = 'Danske Bank'

        # A category that we can do operations on, includes the danske_bank customer.
        self.banks = eboks.customer.Category()
        self.banks.id = '966'
        self.banks.name = 'Banks'
        self.banks.segment = eboks.customer.Segment.private

    def tearDown(self):
        self.session.logoff()

    def test_get_groups(self):
        groups = eboks.customer.get_groups(self.session, self.danske_bank)
        for group in groups:
            if group.id == '530':
                self.assertEqual(group.name, 'Kontoudskrifter og lister')
            elif group.id == '4298':
                self.assertEqual(group.name, 'Fonds, depot, pantebreve og valuta')
            elif group.id == '4299':
                self.assertEqual(group.name, 'Meddelelser om betalinger')
            elif group.id == '4300':
                self.assertEqual(group.name, 'Oversigter')
            elif group.id == '5098':
                self.assertEqual(group.name, 'BS-oversigt')
            else:
                self.fail()
            self.assertTrue(isinstance(group.is_subscribed, bool))

    def test_get_group_description(self):
        groups = eboks.customer.get_groups(self.session, self.danske_bank)
        desccription = eboks.customer.get_group_description(self.session, groups[0])
        self.assertGreater(len(desccription), 0)

    def test_get_information(self):
        information = eboks.customer.get_information(self.session, self.danske_bank)
        self.assertGreater(len(information), 0)

    def test_get_categories(self):
        self.do_test_get_categories(eboks.customer.Segment.private)
        self.do_test_get_categories(eboks.customer.Segment.public)

    def do_test_get_categories(self, segment):
        categories = eboks.customer.get_categories(self.session, segment)
        self.assertGreater(len(categories), 2)
        for category in categories:
            self.assertTrue(category.id.isdigit())
            self.assertGreater(len(category.name), 0)
            self.assertEqual(category.segment, segment)
            self.assertGreater(category.customer_amount, 0)

    def test_get_from_category(self):
        banks = eboks.customer.get_from_category(self.session, self.banks)
        for customer in banks:
            if customer.id == self.danske_bank.id:
                self.assertEqual(customer.name, self.danske_bank.name)
                return
        self.fail()

    def test_get_subscribed(self):
        self.do_test_get_subscribed(eboks.customer.Segment.private)
        self.do_test_get_subscribed(eboks.customer.Segment.public)

    def do_test_get_subscribed(self, segment):
        customers = eboks.customer.get_subscribed(self.session, segment)
        for customer in customers:
            self.assertTrue(customer.is_subscribed)


class TestEboksFolder(unittest.TestCase):
    """ Class for testing eboks methods that operate on folders.

    Before each test we create some test folders, use these for testing! The test folder names are

        [test_folder, test_subfolder]

    If by some chance folders with these names already exist, the unit tests will fail.
    """

    def setUp(self):
        self.session = eboks.EboksSession()
        self.session.logon()
        self.topfolder_name = 'test_folder'
        self.subfolder_name = 'test_subfolder'
        self.topfolder = eboks.folder.create(self.session, self.topfolder_name)
        self.subfolder = eboks.folder.create(self.session, self.subfolder_name, self.topfolder)

    def tearDown(self):
        self.session.logoff()
        self.session.logon()
        eboks.folder.delete(self.session, self.topfolder)
        self.session.logoff()

    def test_folder_create(self):
        self.assertTrue(self.topfolder.id.isdigit())
        self.assertEqual(self.topfolder.name, self.topfolder_name)
        self.assertEqual(self.topfolder.type, eboks.folder.Type.custom_folder)
        self.assertEqual(self.topfolder.messages_unread, 0)
        self.assertEqual(self.topfolder.parent_folder_id, '0')
        self.assertTrue(self.subfolder.id.isdigit())
        self.assertEqual(self.subfolder.name, self.subfolder_name)
        self.assertEqual(self.subfolder.type, eboks.folder.Type.custom_subfolder)
        self.assertEqual(self.subfolder.messages_unread, 0)
        self.assertEqual(self.subfolder.parent_folder_id, self.topfolder.id)

    def test_get_folders(self):
        folders = eboks.folder.get_folders(self.session)
        topfolder_found, subfolder_found = False, False
        for folder in folders:
            if folder.id == self.topfolder.id:
                self.assertEqual(folder.parent_folder_id, self.topfolder.parent_folder_id)
                self.assertEqual(len(folder.sub_folder_ids), 1)
                self.assertEqual(folder.sub_folder_ids[0], self.subfolder.id)
                topfolder_found = True
            if folder.id == self.subfolder.id:
                self.assertEqual(folder.parent_folder_id, self.subfolder.parent_folder_id)
                self.assertEqual(len(folder.sub_folder_ids), 0)
                subfolder_found = True
        self.assertTrue(topfolder_found)
        self.assertTrue(subfolder_found)

    def test_get_message_total(self):
        total_folder = eboks.folder.get_message_total(self.session, self.topfolder)
        total_subfolder = eboks.folder.get_message_total(self.session, self.subfolder)
        self.assertEqual(total_folder, 0)
        self.assertEqual(total_subfolder, 0)

    def test_rename_folder(self):
        rootfolder_newname = 'hello world'
        subfolder_newname = 'goodbye world'
        eboks.folder.rename(self.session, self.topfolder, rootfolder_newname)
        eboks.folder.rename(self.session, self.subfolder, subfolder_newname)
        topfolder_ok, subfolder_ok = False, False
        for folder in eboks.folder.get_folders(self.session):
            if folder.id == self.topfolder.id:
                self.assertEqual(folder.type, eboks.folder.Type.custom_folder)
                self.assertEqual(folder.name, rootfolder_newname)
                topfolder_ok = True
            if folder.id == self.subfolder.id:
                self.assertEqual(folder.type, eboks.folder.Type.custom_subfolder)
                self.assertEqual(folder.name, subfolder_newname)
                subfolder_ok = True
        self.assertTrue(topfolder_ok)
        self.assertTrue(subfolder_ok)

    def test_move_folder(self):
        # Move subfolder such that it becomes a top level folder.
        eboks.folder.move(self.session, self.subfolder, None)
        for folder in eboks.folder.get_folders(self.session):
            if folder.id == self.subfolder.id:
                self.assertEqual(folder.type, eboks.folder.Type.custom_folder)
                self.assertEqual(folder.parent_folder_id, '0')
            if folder.id == self.topfolder.id:
                self.assertNotIn(self.subfolder.id, folder.sub_folder_ids)

        # Move subfolder back into the original folder.
        eboks.folder.move(self.session, self.subfolder, self.topfolder)
        for folder in eboks.folder.get_folders(self.session):
            if folder.id == self.subfolder.id:
                self.assertEqual(folder.type, eboks.folder.Type.custom_subfolder)
                self.assertEqual(folder.parent_folder_id, self.topfolder.id)
            if folder.id == self.topfolder.id:
                self.assertIn(self.subfolder.id, folder.sub_folder_ids)

    def test_find_folder_by_name(self):
        rootfolder = eboks.folder.find_by_name(self.session, self.topfolder_name,
                                               eboks.folder.Type.custom_folder)
        self.assertEqual(rootfolder.name, self.topfolder.name)
        self.assertEqual(rootfolder.id, self.topfolder.id)
        subfolder = eboks.folder.find_by_name(self.session, self.subfolder_name,
                                              eboks.folder.Type.custom_subfolder, rootfolder)
        self.assertEqual(subfolder.name, self.subfolder.name)
        self.assertEqual(subfolder.id, self.subfolder.id)

    def test_find_folder_by_name_not_found(self):
        self.assertRaises(
            LookupError, 
            eboks.folder.find_by_name, 
            self.session, 
            "badnamedude", 
            eboks.folder.Type.inbox
        )

    def test_find_folder_by_id(self):
        folder = eboks.folder.find_by_id(self.session, self.topfolder.id)
        self.assertEqual(folder.name, self.topfolder.name)
        subfolder = eboks.folder.find_by_id(self.session, self.subfolder.id)
        self.assertEqual(subfolder.name, self.subfolder.name)

    def test_find_folder_by_id_not_found(self):
        self.assertRaises(LookupError, eboks.folder.find_by_id, self.session, "00000000")

    def test_unallowed_folder_operations(self):
        # We do not allow moving, renaming or deleting default folders (inbox, delete items, etc.)
        inbox = eboks.folder.find_by_name(self.session, "Inbox", eboks.folder.Type.inbox)
        self.assertRaises(ValueError, eboks.folder.rename, self.session, inbox, "Inbox")
        self.assertRaises(ValueError, eboks.folder.move, self.session, inbox, self.topfolder)
        self.assertRaises(ValueError, eboks.folder.delete, self.session, inbox)

    def test_deleting_non_empty_folder(self):
        message = eboks.message.upload_message(self.session, self.topfolder, "filename", "txt", "Hello World!")
        try:
            self.assertRaises(ValueError, eboks.folder.delete, self.session, self.topfolder)
        finally:
            # Always delete the created e-boks message.
            eboks.message.delete(self.session, message)

class TestEboksAttachment(unittest.TestCase):
    """ Class for testing eboks methods that operate on attachments.

    We can't really test it, it's impossible (?) to upload a message with attachments by ourselves. We only
    test that the get attachment request returns a successful http response.
    """

    def test_get_attachments(self):
        session = eboks.session.EboksSession()
        session.logon()
        messages = eboks.message.get_latest(session)
        eboks.attachment.get_attachments(session, messages[0])
        eboks.attachment.get_attachments(session, messages[1])
        session.logoff()


class TestEboksMessage(unittest.TestCase):
    """ Class for testing eboks methods that operate on messages.

    Before each test we create some test folders, use these for testing! The test folder names are

        [test_folder_one, test_folder_two]

    If by some chance folders with these names already exist, the unit tests will fail. We also create a single file
    in test folder one that you can use for testing.
    """

    def setUp(self):
        self.session = eboks.EboksSession()
        self.session.logon()

        self.folder_one_name = 'test_folder_one'
        self.folder_two_name = 'test_folder_two'
        self.folder_one = eboks.folder.create(self.session, self.folder_one_name)
        self.folder_two = eboks.folder.create(self.session, self.folder_two_name)

        self.message_name = 'testtitle'
        self.message_filetype = 'txt'
        self.message_content = 'content'
        self.message = eboks.message.upload_message(
            self.session,
            self.folder_one,
            self.message_name,
            self.message_filetype,
            self.message_content
        )

    def tearDown(self):
        self.session.logoff()
        self.session.logon()
        eboks.message.delete(self.session, self.message)
        eboks.folder.delete(self.session, self.folder_one)
        eboks.folder.delete(self.session, self.folder_two)
        self.session.logoff()

    def test_get_latest(self):
        message_list = eboks.message.get_latest(self.session)
        self.assertTrue(find_message_in_list(self.message, message_list))

    def test_get_unread(self):
        eboks.message.set_read(self.session, self.message, False)
        message_list = eboks.message.get_unread(self.session)
        self.assertTrue(find_message_in_list(self.message, message_list))

    def test_get_from_period(self):
        tomorrow = datetime.datetime.today() + datetime.timedelta(days=1)
        yesterday = datetime.datetime.today() - datetime.timedelta(days=1)
        message_list = eboks.message.get_from_period(self.session, yesterday, tomorrow)
        self.assertTrue(find_message_in_list(self.message, message_list))

    def test_get_from_sender(self):
        message_list = eboks.message.get_from_sender(self.session, eboks.sender.get_myself())
        self.assertTrue(find_message_in_list(self.message, message_list))

    def test_get_from_folder(self):
        message_list = eboks.message.get_from_folder(self.session, self.folder_one)
        self.assertTrue(find_message_in_list(self.message, message_list))

    def test_search(self):
        message_list = eboks.message.search(self.session, self.message_name)
        self.assertTrue(find_message_in_list(self.message, message_list))

    def test_get_content(self):
        text = eboks.message.get_content(self.session, self.message)
        self.assertEqual(text, self.message_content)

    def test_get_content_bytes(self):
        byte_array = eboks.message.get_content_bytes(self.session, self.message)
        self.assertEqual(byte_array, self.message_content.encode())

    def test_upload_message(self):
        self.assertGreater(len(self.message.id), 0)
        self.assertEqual(self.message.name, self.message_name)
        self.assertEqual(self.message.unread, False)
        self.assertEqual(self.message.receive_datetime, self.message.receive_datetime)
        self.assertEqual(self.message.folder_id, self.folder_one.id)
        self.assertEqual(self.message.size_bytes, len(self.message_content))
        self.assertEqual(self.message.filetype, self.message_filetype)
        self.assertEqual(self.message.sender_id, eboks.sender.get_myself().id)
        self.assertEqual(self.message.sender_name, eboks.sender.get_myself().name)
        self.assertEqual(self.message.attachments_count, 0)
        self.assertEqual(self.message.note, '')

    def test_upload_file(self):
        # Create temp file.
        filename = 'test_eboks.temp'
        content = 'content in file'
        filetype = 'txt'
        with open(filename, 'w') as file:
            file.write(content)

        # Upload file and check that it is ok.
        message = eboks.message.upload_file(self.session, self.folder_two, filename, filetype)
        try:
            self.assertGreater(len(message.id), 0)
            self.assertEqual(message.name, filename)
            self.assertEqual(message.unread, False)
            self.assertEqual(message.folder_id, self.folder_two.id)
            self.assertEqual(message.size_bytes, len(content))
            self.assertEqual(message.filetype, filetype)
            self.assertEqual(message.sender_id, eboks.sender.get_myself().id)
            self.assertEqual(message.sender_name, eboks.sender.get_myself().name)
            self.assertEqual(message.attachments_count, 0)
            self.assertEqual(message.note, '')
            self.assertEqual(eboks.message.get_content(self.session, message), content)
        finally:
            # Always delete the created e-boks message and the temp file.
            eboks.message.delete(self.session, message)
            os.remove(filename)
            
    def test_delete_not_allowed(self):
        # We only allow deletion of message sent by yourself (sender id 0).
        message = eboks.message.Message;
        message.id = '123456789'
        message.name = 'test name'
        message.sender_id = '1234567'
        message.sender_id = 'test name sender'
        self.assertRaises(ValueError, eboks.message.delete, self.session, message);

    def test_rename(self):
        newname = 'newtitle'
        eboks.message.rename(self.session, self.message, newname)
        self.assertEqual(self.message.name, newname)
        for message in eboks.message.get_latest(self.session):
            if message.id == self.message.id:
                self.assertEqual(message.name, newname)
                
    def test_rename_not_allowed(self):
        # We only allow renaming of message sent by yourself (sender id 0).
        message = eboks.message.Message;
        message.id = '123456789'
        message.name = 'test name'
        message.sender_id = '1234567'
        message.sender_id = 'test name sender'
        self.assertRaises(ValueError, eboks.message.rename, self.session, message, 'new test name');

    def test_move(self):
        eboks.message.move(self.session, self.message, self.folder_two)
        self.assertEqual(self.message.folder_id, self.folder_two.id)
        for message in eboks.message.get_latest(self.session):
            if message.id == self.message.id:
                self.assertEqual(message.folder_id, self.folder_two.id)

    def test_set_note(self):
        note = 'testnote'
        eboks.message.set_note(self.session, self.message, note)
        self.assertEqual(self.message.note, note)
        for message in eboks.message.get_latest(self.session):
            if message.id == self.message.id:
                self.assertEqual(message.note, note)

    def test_set_read(self):
        # Set message as unread
        eboks.message.set_read(self.session, self.message, False)
        self.assertEqual(self.message.unread, True)
        for message in eboks.message.get_latest(self.session):
            if message.id == self.message.id:
                self.assertEqual(message.unread, True)

        # Set message as read
        eboks.message.set_read(self.session, self.message)
        self.assertEqual(self.message.unread, False)
        for message in eboks.message.get_latest(self.session):
            if message.id == self.message.id:
                self.assertEqual(message.unread, False)

    def test_iterate_over_messages(self):
        # Because the lists are small in the above tests, we probably didn't use the skip and take parameters to
        # iterate over a message list. We test it here. We get all message with an 'e' in its name, which should
        # catch a couple of messages. When iterating we only take one message at a time.
        url = '{uri}/0/mail/folder/search?search=e&'.format(uri=self.session.uri)
        found_message_ids = []
        for message in eboks.message.iterate_over_messages(self.session, url, take=30):
            self.assertNotIn(message.id, found_message_ids)
            found_message_ids.append(message.id)


def find_message_in_list(target_message, message_list):
    for message in message_list:
        if message.id == target_message.id:
            return True
    return False


if __name__ == '__main__':
    unittest.main()
