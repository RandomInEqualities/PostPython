import tkinter as tk
import requests
import bs4

import eboks


class EboksInvestigator(tk.Frame):

    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        self.parent = parent
        self.init_ui()

        self.session = eboks.EboksSession()
        self.init_session()

    def init_ui(self):

        self.parent.title("Eboks URL Investigator")
        self.pack(fill=tk.BOTH, expand=True)

        frame_url = tk.Frame(self)
        frame_url.pack(fill=tk.X)

        frame_url_label = tk.Label(frame_url, text="URL", width=7)
        frame_url_label.pack(side=tk.LEFT, padx=5, pady=5)

        self.frame_url_entry = tk.Entry(frame_url)
        self.frame_url_entry.pack(fill=tk.X, padx=5, expand=True)

        frame_info = tk.Frame(self)
        frame_info.pack(fill=tk.X)

        frame_url_label = tk.Label(frame_info, width=7)
        frame_url_label.pack(side=tk.LEFT, padx=5, pady=5)

        self.frame_info_user_id = tk.Label(frame_info, text="UserId: Unknown")
        self.frame_info_user_id.pack(side=tk.LEFT, padx=5, pady=5)

        self.frame_info_user_name = tk.Label(frame_info, text="UserName: Unknown")
        self.frame_info_user_name.pack(side=tk.LEFT, padx=5, pady=5)

        self.frame_info_send_get_button = tk.Button(frame_info, text="Get")
        self.frame_info_send_get_button.pack(side=tk.RIGHT, padx=5, pady=5)

        self.frame_info_send_put_button = tk.Button(frame_info, text="Put")
        self.frame_info_send_put_button.pack(side=tk.RIGHT, padx=5, pady=5)

        self.frame_info_send_delete_button = tk.Button(frame_info, text="Delete")
        self.frame_info_send_delete_button.pack(side=tk.RIGHT, padx=5, pady=5)

        self.frame_info_reconnect_button = tk.Button(frame_info, text="Reconnect")
        self.frame_info_reconnect_button.pack(side=tk.RIGHT, padx=5, pady=5)

        frame_response = tk.Frame(self)
        frame_response.pack(fill=tk.BOTH, expand=True)

        frame_response_label = tk.Label(frame_response, text="Response", width=7)
        frame_response_label.pack(side=tk.LEFT, anchor=tk.N, padx=5, pady=5)

        self.frame_response_text = tk.Text(frame_response)
        self.frame_response_text.pack(fill=tk.BOTH, pady=5, padx=5, expand=True)

    def init_session(self):
        self.session.logon()

        self.frame_info_user_id["text"] = "UserId: {}".format(self.session.userid)
        self.frame_info_user_name["text"] = "UserName: {}".format(self.session.username)

        self.url = tk.StringVar(value=self.session.uri + '/0/mail/folders')
        self.frame_url_entry['textvariable'] = self.url

        self.frame_info_reconnect_button['command'] = self.reconnect
        self.frame_info_send_get_button['command'] = self.send_get_request
        self.frame_info_send_put_button['command'] = self.send_put_request
        self.frame_info_send_delete_button['command'] = self.send_delete_request
        self.frame_url_entry.bind('<Return>', self.send_get_request)

        self.parent.protocol("WM_DELETE_WINDOW", self.close_window)

    def close_window(self):
        self.session.logoff()
        self.parent.destroy()

    def reconnect(self):
        self.session.logoff()
        self.session.logon()
        self.frame_response_text.insert('1.0', 'Reconnected ... \n\n')

    def send_get_request(self, event=None):
        self.send_request(self.session.get)

    def send_put_request(self, event=None):
        self.send_request(self.session.put)

    def send_delete_request(self, event=None):
        self.send_request(self.session.delete)

    def send_request(self, request_function):
        try:
            response = request_function(self.url.get())
        except requests.HTTPError as error:
            response = error.response
        self.show_response(response)

    def show_response(self, response):
        self.frame_response_text.delete('1.0', tk.END)
        self.frame_response_text.insert(tk.END, 'Status Code:\n' + str(response.status_code) + '\n\n')
        self.frame_response_text.insert(tk.END, 'Headers:\n')
        for key, value in response.headers.items():
            self.frame_response_text.insert(tk.END, key + ': ' + value + '\n')
        self.frame_response_text.insert(tk.END, '\nContent:\n')
        content = bs4.BeautifulSoup(response.text, 'html.parser').prettify()
        self.frame_response_text.insert(tk.END, utf8_convert_65536_to_string(content))


def utf8_convert_65536_to_string(string):
    """ Tkinter can not display unicode characters outside the range U+0000-U+FFFF. We convert these characters to
    a text code. """
    string_list = list(string)
    index = 0
    while index < len(string_list):
        o = ord(string_list[index])
        if o > 65535:
            string_list[index] = str(o)
        index += 1
    return ''.join(string_list)


def main():
    root = tk.Tk()
    root.geometry("1000x800+100+100")
    EboksInvestigator(root)
    root.mainloop()


if __name__ == "__main__":
    main()
